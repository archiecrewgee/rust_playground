// constants
const OUT_OF_SCOPE_CONSTANT: u8 = 20;

mod my_crate;


// main function
fn main() {
    println!("Hola amigos");

    let immutable_value = 200;
    println!("Immutable value: {immutable_value}");

    let mut mutable_value = 14;
    print!("Mutable value: {mutable_value} goes to ");
    mutable_value += 1;
    println!("{mutable_value}");

    // note constants must have a type unlike immutable variables
    const CONSTANT: u32 = 15;
    println!("Constant: {CONSTANT}");
    println!("Out of scope constant: {OUT_OF_SCOPE_CONSTANT}");

    // shadowing (basically just replaces previous variable instance)
    let immutable_value = "this is a shadowed 'immutable_value' that also changes the type of the previous";
    let mutable_value = immutable_value.len(); // note this is now immutable
    println!("Shadowing: '{immutable_value}' is of length: {mutable_value}");

    // test types <- note these exist at-compilation but don't need to be specified (i.e. c++ auto keyword is implied)
    let tup: (_, _, _) = (10, 1.0, -43);
    let a = tup.0;
    let b = tup.1;
    let c = tup.2;

    println!("Tuple w/ auto generated types is: {a}, {b}, {c}");

    let int_array: [i32; 6] = [0, 1, 2, 3, 4, 5];   // int array of len 6
    let zero_array = [0; 5]; // zero array five in len

    let mut fifth_element = int_array[4];
    println!("The fifth element is: {fifth_element}");
    fifth_element = zero_array[4];
    println!("The fifth element is: {fifth_element}");

    // functions
    let v = 0;          // note that immutability can be lost as argument
    statement(v);
    
    let mut v_temp = v;
    let v_new = loop {
        v_temp = expresion(v_temp);
        println!("new v is {v_temp}");

        if v_temp > 20 {
            break v_temp;
        } 
    };

    // this is the same as while x {};
    // also for x in b {} <- for range use (start..stop).rev()
    // loops can be given a name by placing 'this is my loop': loop {}
    // these can be broken to with break 'this is my loop'

    println!("final v is {v_new}");

    // classes / structs
    let mut rectangle = my_crate::a_module::Rectangle {
        width: 30,
        height: 100,
    };

    println!("the rectangle area is {}", rectangle.area());
}

// functions
fn statement(a: u32) {
    println!("Statements do not return anything | a: {a}");
}

fn expresion(mut a: u32) -> u32 {   // note that mutability must be specified
    println!("Expressions return something | ret: {a} + 1");
    a += 1;
    a
    // a + 1 // note no semicolon here
}